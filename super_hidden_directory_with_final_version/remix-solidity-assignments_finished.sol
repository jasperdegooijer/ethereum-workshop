pragma solidity ^0.4.22;

contract Auction {
    address public beneficiary;
    uint public auctionEnd; // one week from now

    address public highestBidder;
    uint public highestBid;

    /*
    Mappings are standard set to private and can't be accessed like previous variables declared.
    Assignment 1a: Make pendingReturns uint values accessible.
    Assignment 1b: What if we only want address owners to be allowed to see their own pending returns?.
    */

    // 1a is best solved as follows:
    // mapping(address => uint) public pendingReturns;
    
    // For 1b we need to get a bit smarted with it (1b start):
    
    // Keep the mapping private
    mapping(address => uint) pendingReturns;

    // Create a getter function
    function getPendingReturns() public view returns(uint _pendingReturnsInWei) {
        return pendingReturns[msg.sender];
    }

    // 1b finished

    bool ended;

    event HighestBidIncreased(address bidder, uint amount);
    
    /*
    Assignment 2: Create and emit an event fired when the auction is finished broadcasting the winning address and the amount won.
    */
    event AuctionEnded(address winner, uint amount);
    // last line in the auctionEnd function for the emitting of the event.
    
    /* 
    Assignment 3a: Make the uint `auctionEnd` settable in the constructor (at contract deployment) instead of fixed tot 1 week.
    Assignment 3b: Make the address `beneficiary` settable in the constructor (at contract deployment) instead of always being set to the contract deployer.
    */
    constructor(
        uint _biddingTime,
        address _beneficiary
    ) public {
        beneficiary = _beneficiary;
        auctionEnd = now + _biddingTime;
    }

    function bid() public payable {
        require(
            now <= auctionEnd,
            "Auction already ended."
        );
        
        require(
            msg.value > highestBid,
            "There already is a higher bid."
        );

        if (highestBid != 0) {
            pendingReturns[highestBidder] += highestBid;
        }
        highestBidder = msg.sender;
        highestBid = msg.value;
        emit HighestBidIncreased(msg.sender, msg.value);
    }

    function withdraw() public returns (bool) {
        uint amount = pendingReturns[msg.sender];
        if (amount > 0) {

            pendingReturns[msg.sender] = 0;

            if (!msg.sender.send(amount)) {
                pendingReturns[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }
    
    function auctionEnd() public {
        /*
        1. Conditions 
        */
        require(now >= auctionEnd, "Auction not yet ended.");
        require(!ended, "auctionEnd has already been called.");

        /* 
        2. Effects 
        */
        ended = true;

        /*
        3. Interaction 
        */
        beneficiary.transfer(highestBid);

        emit AuctionEnded(highestBidder,  highestBid);
    }
}